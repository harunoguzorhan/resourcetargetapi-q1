package com.harunoguzorhan.q1;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Api {

	public static Scanner scanner = new Scanner(System.in);
	public static boolean isMapped = false;
	public static HashMap<String, String> map = null;
	public static boolean showErrorMessage = false;

	public static void populateTarget(Resource resource, Target target) {

		Method[] targetMethods = target.getClass().getDeclaredMethods();
		Method[] resourceMethods = resource.getClass().getDeclaredMethods();

		for (Map.Entry<String, String> entry : map.entrySet()) {
			for (Method method : targetMethods) {
				Method[] getterAndSetter = findGetterAndSetterForField(entry,
						resourceMethods, targetMethods);
				if (getterAndSetter[0] != null && getterAndSetter[1] != null) {
					try {
						String getterType = getterAndSetter[0].getReturnType()
								.getName();
						String setterType = getterAndSetter[1]
								.getParameterTypes()[0].getName();

						if (!getterType.equals(setterType)) {
							if (showErrorMessage) {
								System.out.println(entry.getKey() + " ("
										+ getterType + ")"
										+ " doesnt match with "
										+ entry.getValue() + " (" + setterType
										+ ")");
							}

						} else {
							getterAndSetter[1].invoke(target,
									getterAndSetter[0].invoke(resource));

							System.out.println(entry.getKey() + " --> "
									+ entry.getValue() + " completed!");

						}
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				}
			}
		}

	}

	private static Method[] findGetterAndSetterForField(
			Entry<String, String> entry, Method[] resourceMethods,
			Method[] targetMethods) {
		Method[] getterAndSetter = new Method[2];
		for (Method resourceMethod : resourceMethods) {
			if (resourceMethod.getName().equals(makeGetterName(entry.getKey()))) {
				getterAndSetter[0] = resourceMethod;
				break;
			}
		}

		for (Method targetMethod : targetMethods) {
			if (targetMethod.getName().equals(makeSetterName(entry.getValue()))) {
				getterAndSetter[1] = targetMethod;
				break;
			}
		}
		return getterAndSetter;
	}

	private static Object makeSetterName(String key) {
		return "set" + Character.toUpperCase(key.charAt(0)) + key.substring(1);
	}

	private static String makeGetterName(String key) {
		return "get" + Character.toUpperCase(key.charAt(0)) + key.substring(1);
	}

	public static void mapFieldsResourceToTarget(Object resource, Object target) {
		isMapped = true;
		map = new HashMap<String, String>();
		Field[] fields = resource.getClass().getDeclaredFields();
		for (Field field : fields) {
			String targetField = getCommandFromUser("Where do you want to map the field '"
					+ field.getName() + "'?");
			map.put(field.getName(), targetField);

		}
	}

	public static String getCommandFromUser(String message) {
		System.out.println(message);
		String cmd = scanner.nextLine();
		return cmd;
	}

	public static void mapFieldsResourceToTargetAutomatically(
			Resource resource, Target target) {
		isMapped = true;
		map = new HashMap<String, String>();
		Field[] fields = resource.getClass().getDeclaredFields();
		for (Field field : fields) {
			map.put(field.getName(), field.getName());
		}
	}

}
