package com.harunoguzorhan.q1;

public class Test {

	public static void main(String[] args) {
		Resource resource = new Resource();
		resource.setId(1);
		resource.setKey(1L);
		resource.setResourceValue("Resource");
		Target target = new Target();
		System.out.println("Before call api");
		System.out.println(resource);
		System.out.println(target);
		if (Api.getCommandFromUser(
				"Do you want to map resource fields to target fields manually? (y/n?)")
				.equalsIgnoreCase("Y")) {
			Api.mapFieldsResourceToTarget(resource, target);
		} else {
			Api.mapFieldsResourceToTargetAutomatically(resource, target);
		}

		if (Api.getCommandFromUser(
				"Mapping is completed!\n Do you want to see a message in case of mismatch of data type(y/n)")
				.equalsIgnoreCase("Y")) {
			Api.showErrorMessage = true;
		}

		Api.populateTarget(resource, target);
		System.out.println("After call api");
		System.out.println(resource);
		System.out.println(target);

	}

}
